from django.shortcuts import render
from .models import Book,Author
# Create your views here.

def index(request):
    books = Book.objects.all()
    authors = Author.objects.all()
    return render(request,'miprimerapp/books_list.html',{'books':books,'authors':authors})
