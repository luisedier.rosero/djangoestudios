# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.

class Author(models.Model):
    name = models.CharField(max_length=50)

    def __unicode__(self):
        return self.name

class Book(models.Model):
    name = models.CharField(max_length=50,verbose_name="Libro")
    author = models.ForeignKey(Author,blank=True,null=True,verbose_name="Autor")
    created_at = models.DateField(blank=True,null=True,verbose_name="Fecha Creación")

    def __unicode__(self):
        return self.name

    class Meta:
        permissions = (
            ("can_create_book","Permitir crear libro"),
        )
